import React from "react";
import {
    Link
} from "react-router-dom";
import './index.scss'
import {HEADER_NAVIGATION_ITEMS} from "../../utils/constatns";
import Logo from "../logo";

function Header(props) {
    return (
        <div className="header">
            <div className="row">
                <div className="col col-large-3 col-medium-3 col-small-12">
                    {props.logo && (<Logo/>)}
                </div>
                <div className="col col-large-9 col-medium-9 col-small-12">
                    <div className="navigation">
                        {HEADER_NAVIGATION_ITEMS.map(item => {
                            return (
                                <Link to={item.href}>{item.name}</Link>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header;
