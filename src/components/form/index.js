import React, {Component} from "react";
import './index.scss';

class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {

            },
            error: false
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        if (typeof this.props.submit === 'function') {
            const data = {};
            const state = this.state.data;
            let error = false;
            this.props.fields.forEach(field => {
                if (!state[field.name] || !state[field.name].value) {
                    if (!field.value && field.required) {
                        state[field.name] = {
                            value: '',
                            error: true,
                        }
                    } else {
                        state[field.name] = {
                            value: field.value,
                            error: false,
                        }
                    }
                }

                if (state[field.name].error) {
                    error = true;
                }

                data[field.name] = state[field.name].value;
            });

            this.setState({
                data: state,
                error: error
            });

            if (!error) {
                this.props.submit(data);
            }
        }
    };

    handleCancel() {
        this.setState({
            data: {}
        });
        if (typeof this.props.cancel === 'function') {
            this.props.cancel();
        }
    }

    handleChange(name, value) {
        const data = this.state.data;

        data[name] = {
            error: false,
            value: value
        };

        if (!value) {
            this.props.fields.forEach(field => {
                if (field.name === name) {
                    if (field.required) {
                        data[name].error = true;
                    }
                }
            });
        }

        this.setState({
            data: data
        });
    }

    render() {
        return (
            <form className="form"
                  onSubmit={(e) => {
                      this.handleSubmit(e);
                  }}>
                <h3 className="form-header">{this.props.title}</h3>
                <div className="form-body">
                    {this.props.fields.map((field, key) => {
                        const dataItem = this.state.data[field.name];
                        if (field.type === 'textarea') {
                            return (
                                <div  key={key}
                                      className="form-field">
                                <textarea
                                    className={`form-input textarea ${(dataItem && dataItem.error) ? "error" : ""}`}
                                    name={field.name}
                                    placeholder={field.label}
                                    value={(dataItem && dataItem.value) || ''}
                                    onChange={(e) => {
                                        this.handleChange(field.name, e.target.value)
                                    }}/>
                                </div>
                            )
                        }

                        return (
                            <div key={key}
                                 className={"form-field field-" + field.type}>
                                <input
                                    className={`form-input ${(dataItem && dataItem.error) ? "error" : ""}`}
                                    name={field.name}
                                    placeholder={field.label}
                                    value={(dataItem && dataItem.value) || ''}
                                    onChange={(e) => {
                                        this.handleChange(field.name, e.target.value)
                                    }}
                                    type={field.type}/>
                            </div>
                        )
                    })}
                </div>
                {this.props.message && (
                    this.props.message
                )}

                {this.state.error && (
                    <div className="form-result error">
                        {"Please fill out all required fields."}
                    </div>
                )}
                <div className="form-footer">
                    <button className="form-button submit"
                            onClick={(e) => {
                                this.handleSubmit(e);
                            }}
                            type="submit">
                        {this.props.submitText || "Submit"}
                    </button>
                    {this.props.showCancel && (
                        <button className="form-button reset"
                                onClick={(e) => {
                                    this.handleCancel(e)
                                }}
                                type="reset">
                            {"Cancel"}
                        </button>
                    )}
                </div>
            </form>
        )
    }
}

export default Form;


