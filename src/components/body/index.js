import React from "react";
import './index.scss'

function Body(props) {
    return (
        <div className="body">
            {props.children}
        </div>
    )
}

export default Body;
