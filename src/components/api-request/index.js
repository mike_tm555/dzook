import React, {useCallback, useState} from "react";
import Form from "../form";
import {API_REQUEST_FIELDS, API_REQUEST_HOST} from "../../utils/constatns";
import Modal from "../modal";
import {Post} from "../../utils/helpers";
import './index.scss';

function ApiRequest(props) {
    const [result, setResult] = useState(false);

    const handleSubmit = useCallback((data) => {
        Post(API_REQUEST_HOST, {
            ...data,
        }).then((data) => {
            setResult({
                type: 'success',
                message: 'Thank you for the request. We will respond to you within 24 hours.'
            });
        }).catch(e => {
            setResult({
                type: 'error',
                message: 'Oops, something went wrong, please try again.\nPlease fill all required fields.'
            });
        });
    }, []);

    const toggleModal = useCallback((status) => {
        if (typeof props.toggleModal === 'function') {
            props.toggleModal(status);
        }
    }, [props]);

    return (
        <Modal
            width={"60vw"}
            maxHeight={"80vh"}
            maxWidth={"800px"}
            classList="api-request-modal"
            close={() => {
                toggleModal(false);
            }}>
            <div className="row">
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <Form title={"Write us if you would like to use our API"}
                          showCancel={true}
                          cancel={() => {
                              toggleModal(false);
                          }}
                          submit={(data) => {
                              handleSubmit(data);
                          }}
                          message={result && (<p className={"form-result " + result.type}>{result.message}</p>)}
                          fields={API_REQUEST_FIELDS}/>
                </div>
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <div className="banner-box">
                        <div className="api-request-banner-wrapper">
                            <h3 className="banner-title">{"Hey! If you want to use my API just write me an e-mail:)"}</h3>
                            <div className="api-request-banner"/>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default ApiRequest;
