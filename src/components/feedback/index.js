import React, {useCallback, useState} from "react";
import Form from "../form";
import {FEEDBACK_FIELDS, FEEDBACK_HOST} from "../../utils/constatns";
import Modal from "../modal";
import {Post} from "../../utils/helpers";
import './index.scss';

function FeedBack(props) {
    const [result, setResult] = useState(false);

    const handleSubmitFeedBack = useCallback((data) => {
        Post(FEEDBACK_HOST, {
            ...data,
        }).then((data) => {
            setResult({
                type: 'success',
                message: 'Your feedback has been sent successfully!'
            });
        }).catch(e => {
            setResult({
                type: 'error',
                message: 'Oops, something went wrong, please try again.\nPlease fill all required fields.'
            });
        });
    }, []);

    const toggleFeedBackModal = useCallback((status) => {
        if (typeof props.toggleModal === 'function') {
            props.toggleModal(status);
        }
    }, [props]);

    return (
        <Modal
            width={"60vw"}
            maxHeight={"80vh"}
            maxWidth={"800px"}
            classList="feedback-modal"
            close={() => {
                toggleFeedBackModal(false);
            }}>
            <div className="row">
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <Form title={"Leave your feedback"}
                          showCancel={true}
                          cancel={() => {
                              toggleFeedBackModal(false);
                          }}
                          submit={(data) => {
                              handleSubmitFeedBack(data);
                          }}
                          message={result && (<p className={"form-result " + result.type}>{result.message}</p>)}
                          fields={FEEDBACK_FIELDS}/>
                </div>
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <div className="banner-box">
                        <div className="feedback-banner-wrapper">
                            <h3 className="banner-title">{"I'm still learning so your feedback will help me to improve my skills:) \n thank you!"}</h3>
                            <div className="feedback-banner"/>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default FeedBack;
