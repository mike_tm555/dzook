import React from "react";
import './index.scss';
import {PAGES_NAVIGATION_ITEMS} from "../../utils/constatns";
import {Link} from "react-router-dom";

function Navigation(props) {
    return (
        <div className="pages-navigation">
            {PAGES_NAVIGATION_ITEMS.map((item, key) => {
                return (
                    <Link
                        key={key}
                        to={item.link}
                        className="page-navigation-item">
                        {item.name}
                    </Link>
                )
            })}
        </div>
    )
}

export default Navigation;
