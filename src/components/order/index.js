import React, {useCallback, useState} from "react";
import './index.scss';
import Form from "../form";
import {IMAGES_PATH, ORDER_FIELDS, ORDER_HOST, ORDER_ITEMS} from "../../utils/constatns";
import Modal from "../modal";
import {Post} from "../../utils/helpers";

function Order(props) {
    const [result, setResult] = useState(false);

    const handleSubmit = useCallback((data) => {
        Post(ORDER_HOST, {
            ...data,
        }).then((data) => {
            setResult({
                type: 'success',
                message: 'Thank you for the request. We will respond to you within 24 hours.'
            });
        }).catch(e => {
            setResult({
                type: 'error',
                message: 'Oops, something went wrong, please try again.\nPlease fill all required fields.'
            });
        });
    }, []);

    const toggleModal = useCallback((status) => {
        if (typeof props.toggleModal === 'function') {
            props.toggleModal(status);
        }
    }, [props]);

    const fields = ORDER_FIELDS.map(field => {
        if (field.name === 'image') {
            field.value = props.image;
        }

        return field;
    });

    return (
        <Modal
            width={"65vw"}
            maxHeight={"85vh"}
            maxWidth={"800px"}
            classList="order-modal"
            close={() => {
                toggleModal(false);
            }}>
            <div className="row">
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <div className="order-items mobile-order-items">
                        {ORDER_ITEMS.map((item,index)=> {
                            return (
                                <div key={index}
                                     className="order-item">
                                    <img alt={item.name}
                                         src={`${IMAGES_PATH + item.image}`}/>
                                    <p>{item.name}</p>
                                </div>
                            )
                        })}
                    </div>

                    <Form showCancel={true}
                          title={"Write us email to order products with your illustrated Portrait!"}
                          cancel={() => {
                              toggleModal(false);
                          }}
                          submit={(data) => {
                              handleSubmit(data);
                          }}
                          message={result && (<p className={"form-result " + result.type}>{result.message}</p>)}
                          fields={fields}/>
                </div>
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <div className="banner-box">
                        <div className="order-banner-wrapper">
                            <h3 className="banner-title">{"Hey! You can order your portrait printed on these products! Just write me:)"}</h3>
                            <div className="order-banner"/>
                            <div className="order-items">
                                {ORDER_ITEMS.map((item,index)=> {
                                    return (
                                        <div key={index}
                                             className="order-item">
                                            <img alt={item.name}
                                                 src={`${IMAGES_PATH + item.image}`}/>
                                            <p>{item.name}</p>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default Order;
