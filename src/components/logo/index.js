import React from "react";
import {
    Link
} from "react-router-dom";
import './index.scss'
import {IMAGES_PATH} from "../../utils/constatns";

function Logo() {
    return (
        <Link className="logo-link"
              to="/">
            <img className="logo"
                 src={`${IMAGES_PATH}logo.png`}
                 alt={"Logo"}/>
        </Link>
    )
}

export default Logo;
