import React from "react";
import './index.scss'

function Loader() {
    return (
        <div className="loader">
            <div className="loader-image"/>
        </div>
    )
}

export default Loader;
