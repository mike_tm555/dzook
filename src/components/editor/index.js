import React, {useCallback, useState} from "react";
import './index.scss';
import {Post} from "../../utils/helpers";
import {DRAW_HOST} from "../../utils/constatns";
import Drawer from "../drawer";
import Logo from "../logo";
import Loader from "../loader";
import FeedBack from "../feedback";
import Edit from "../edit";
import Order from "../order";
import ApiRequest from "../api-request";

function Editor(props) {
    const [photo, setPhoto] = useState(null);
    const [draw, setDraw] = useState('empty');
    const [showFeedbackModal, setShowFeedbackModal] = useState(false);
    const [showOrderModal, setShowOrderModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [showApiRequestModal, setShowApiRequestModal] = useState(false);

    const handleUploadPhoto = useCallback((e) => {
        const file = e.target.files[0];
        const reader = new FileReader();

        reader.onload = ((theFile) => {
            const file = theFile.target.result;
            if (file.indexOf('jpg') > -1 || file.indexOf('jpeg') > -1 || file.indexOf('png') > -1) {
                setPhoto(theFile.target);
                setDraw('empty');
            }
        });
        reader.readAsDataURL(file);
    }, [setPhoto]);

    const cleanBase64 = useCallback((photoImage) => {
        photoImage = photoImage.replace("data:image/jpeg;base64,", "");
        photoImage = photoImage.replace("data:image/png;base64,", "");
        photoImage = photoImage.replace("data:image/gif;base64,", "");
        photoImage = photoImage.replace("data:image/bmp;base64,", "");

        return photoImage;
    }, []);

    const handleSetDraw = useCallback((e) => {
        if (photo) {
            document.querySelector('.edit-photo-box').style.display = 'block';
            document.documentElement.scrollTop = document.querySelector('.edit-photo-box').getBoundingClientRect().top;

            setDraw('loading');

            const photoImage = cleanBase64(photo.result);

            Post(DRAW_HOST, {
                image: photoImage
            }).then((data) => {
                setDraw({
                    path: data.data.path,
                    result: data.data.result,
                });
            }).catch(e => {
                setDraw('error');
            });
        }
    }, [setDraw, photo, cleanBase64]);

    const openFeedBackModal = useCallback(() => {
        setShowFeedbackModal(true);
    }, [setShowFeedbackModal]);

    const openOrderModal = useCallback(() => {
        setShowOrderModal(true);
    }, [setShowOrderModal]);

    const openEditModal = useCallback(() => {
        setShowEditModal(true);
    }, [setShowEditModal]);

    const openApiRequestModal = useCallback(() => {
        setShowApiRequestModal(true);
    }, [setShowApiRequestModal]);

    let editorContent = null;

    switch(draw) {
        case "empty": {
            editorContent = (
                <div className="editor-content empty"/>
            );
            break;
        }
        case "error": {
            editorContent = (
                <div className="editor-content error"/>
            );
            break;
        }
        case "loading": {
            editorContent = (
                <div className="editor-content loading">
                    <Loader/>
                </div>
            );
            break;
        }
        default: {
            editorContent = (
                <Drawer
                    draw={draw}
                    openFeedBackModal={openFeedBackModal}
                    openOrderModal={openOrderModal}
                    openEditModal={openEditModal}
                    openApiRequestModal={openApiRequestModal}
                />
            );
            break;
        }
    }

    return (
        <>
            <div className="editor">
                <div className="row">
                    <div className="col col-large-3 col-medium-12 col-small-12">
                        <div className="upload-photo-section">
                            <div className="logo-box">
                                <Logo/>
                            </div>
                            <div className="mobile-title-box">
                                <h2>{"Hey!"}</h2>
                                <h2>{"Get Your Stylish Illustrated"}</h2>
                                <h2>{"Portrait Now!"}</h2>
                            </div>
                            <div className="upload-photo-box">
                                <div className="add-photo-button-box">
                                    <label htmlFor="upload-input"
                                           className="upload-button">
                                        <span className="upload-icon"><i className="fa fa-plus"/></span>
                                        <span className="upload-label">{"Add your photo"}</span>
                                        <input id="upload-input"
                                               type="file"
                                               onChange={handleUploadPhoto}/>
                                    </label>
                                </div>
                                <div className="photo-box">
                                    {photo ? (
                                        <div className="user-photo"
                                             style={{backgroundImage: `url(${photo.result})`}}>
                                        </div>
                                    ) : (
                                        <>
                                            <div className="empty-photo">
                                                <div className="mark-section">
                                                    <span className="mark-icon"><i className="fa fa-heart"/></span>
                                                    <span className="mark-label">{"Free Cool Portrait!"}</span>
                                                </div>
                                                <div className="mark-section">
                                                    <span className="mark-icon"><i className="fa fa-heart"/></span>
                                                    <span className="mark-label">{"Change Background!"}</span>
                                                </div>
                                                <div className="mark-section">
                                                    <span className="mark-icon"><i className="fa fa-heart"/></span>
                                                    <span className="mark-label">{"Share!"}</span>
                                                </div>
                                            </div>
                                            <div className="empty-photo-mobile">
                                                <h4 className="mark-title">{"To get better result, please"}</h4>
                                                <div className="mark-section">
                                                    <span className="mark-label">{"- Free Cool Portrait!"}</span>
                                                </div>
                                                <div className="mark-section">
                                                    <span className="mark-label">{"- Change Background!"}</span>
                                                </div>
                                                <div className="mark-section">
                                                    <span className="mark-label">{"- Share!"}</span>
                                                </div>
                                            </div>
                                        </>
                                    )}
                                </div>
                                <div className="draw-button-box">
                                    <button onClick={handleSetDraw}
                                            className={`draw-button ${(!photo || draw.path) && "disabled"}`}>
                                        <span className="draw-label">{"Draw Me"}</span>
                                    </button>
                                </div>
                            </div>
                            {(draw && draw.path) && (
                                <p className="feedback-button"
                                   onClick={() => {
                                       openFeedBackModal(true);
                                   }}>
                                    {"Leave Your feedback"}
                                </p>
                            )}
                        </div>
                    </div>
                    <div className="col col-large-9 col-medium-12 col-small-12">
                        <div className="edit-photo-section">
                            <div className="edit-photo-box">
                                {editorContent}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {showFeedbackModal && (
                <FeedBack toggleModal={setShowFeedbackModal}/>
            )}

            {showOrderModal && (
                <Order image={draw.path}
                       toggleModal={setShowOrderModal}/>
            )}

            {showEditModal && (
                <Edit image={draw.path}
                      toggleModal={setShowEditModal}/>
            )}

            {showApiRequestModal && (
                <ApiRequest toggleModal={setShowApiRequestModal}/>
            )}
        </>
    )
}

export default Editor;
