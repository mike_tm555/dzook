import React, {Component, createRef} from "react";
import { ChromePicker } from 'react-color';
import {DRAW_DEFAULT_COLORS, IMAGES_PATH, SOCIAL_SHARE_ICONS, UPLOAD_HOST} from "../../utils/constatns";
import Wrapper from "../wrapper";
import { lil } from 'lilo-lala-canvas/dist/lil.min.js';
import './index.scss';
import {Post} from "../../utils/helpers";
import Loader from "../loader";

const LOADER_TIMER = 3000;

class Drawer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            backColor: '#ffffff',
            showColor: false,
            shareImage: null,
            shareLoading: false,
            canvasLoader: true
        };

        this.canvasRef = createRef();
        this.canvas = null;
        this.fakeCanvas = null;
    }

    componentDidMount() {
        const canvasTarget = document.querySelector('.drawer-image-box');
        if (canvasTarget !== null) {
            const targetWidth = canvasTarget.clientWidth;
            const targetHeight = canvasTarget.clientHeight;

            const image = new Image();
            image.src = this.props.draw.result;
            image.onload = () => {
                const imageWidth = image.width;
                const imageHeight = image.height;

                const widthZoom = targetWidth/imageWidth;
                const heightZoom = targetHeight/imageHeight;

                const canvasZoom = Math.min(widthZoom, heightZoom);

                const imageLeft = (targetWidth - imageWidth * canvasZoom);
                const imageTop = targetHeight - imageHeight * canvasZoom;

                this.canvas = new lil.Canvas('drawer-canvas', {
                    width: targetWidth - imageLeft,
                    height: targetHeight - imageTop,
                    selection: false
                });

                this.fakeCanvas = new lil.Canvas('fake-canvas', {
                    width: imageWidth,
                    height: imageHeight
                });

                this.canvas.setBackgroundImage(this.props.draw.result, this.canvas.renderAll.bind(this.canvas), {
                    originX: 'left',
                    originY: 'top',
                    left: 0,
                    top: 0,
                    crossOrigin: 'anonymous'
                });

                this.fakeCanvas.setBackgroundImage(this.props.draw.result, this.fakeCanvas.renderAll.bind(this.fakeCanvas), {
                    originX: 'left',
                    originY: 'top',
                    left: 0,
                    top: 0,
                    crossOrigin: 'anonymous'
                });

                this.canvas.setBackgroundColor(this.state.backColor);
                this.fakeCanvas.setBackgroundColor(this.state.backColor);
                this.canvas.setZoom(canvasZoom);
                this.canvas.renderAll();
                this.fakeCanvas.renderAll();

                setTimeout(() => {
                    this.setState({
                        canvasLoader: false
                    });
                }, LOADER_TIMER)
            }
        }
    }

    toggleColor(status) {
        this.setState({
            showColor: status
        })
    }

    toggleShare(status) {
        return null;
        if (status === true) {
            this.setState({
                shareLoading: true
            });
            Post(UPLOAD_HOST, {
                image: this.cleanBase64(this.fakeCanvas.toDataURL())
            }).then((data) => {
                this.setState({
                    shareImage: data.data.path,
                    shareLoading: false
                })
            });
        } else {
            this.setState({
                shareImage: null,
                shareLoading: false
            });
        }
    }

    cleanBase64(photoImage) {
        photoImage = photoImage.replace("data:image/jpeg;base64,", "");
        photoImage = photoImage.replace("data:image/png;base64,", "");
        photoImage = photoImage.replace("data:image/gif;base64,", "");
        photoImage = photoImage.replace("data:image/bmp;base64,", "");

        return photoImage;
    };


    handleChangeComplete(color) {
        this.setBackColor(color.hex);
    }

    handleDownload() {
        if (typeof this.props.openFeedBackModal === 'function') {
            this.props.openFeedBackModal();
        }

        setTimeout(() => {
            const link = document.createElement('a');
            link.href = this.fakeCanvas.toDataURL();
            link.download = 'file-dzook.png';
            link.click();
        }, 1000);
    }

    handleOpenOrderModal() {
        if (typeof this.props.openOrderModal === 'function') {
            this.props.openOrderModal();
        }
    }

    handleOpenEditModal() {
        if (typeof this.props.openEditModal === 'function') {
            this.props.openEditModal();
        }
    }

    handleOpenApiRequestModal() {
        if (typeof this.props.openApiRequestModal === 'function') {
            this.props.openApiRequestModal();
        }
    }

    setBackColor(color) {
        this.setState({
            backColor: color
        }, () => {
            this.canvas.setBackgroundColor(color);
            this.canvas.renderAll();
            this.fakeCanvas.setBackgroundColor(color);
            this.fakeCanvas.renderAll();
        })
    }

    render() {
        return (
            <div className="drawer">
                {this.state.canvasLoader && (
                    <div className="canvas-loader">
                        <Loader/>
                    </div>
                )}
                <div className="drawer-panel">
                    <div className="drawer-image-box">
                        <canvas
                            ref={this.canvasRef}
                            id="drawer-canvas"/>
                            <div className="drawer-canvas-overlay"/>
                    </div>
                    <div id="fake-canvas-box">
                        <canvas
                            id="fake-canvas"/>
                    </div>
                    <div className="drawer-tools-box">
                        <div className="tools-list">
                            <button className="tool-item"
                                    onClick={() => {
                                        this.handleDownload()
                                    }}>
                                <img className="button-icon" src={`${IMAGES_PATH}download.png`} alt="download"/>
                                <span className="label">{"Save"}</span>
                            </button>
                            <button
                                className="tool-item share-tool"
                                onClick={() => {
                                    this.toggleShare(true);
                                }}>
                                {this.state.shareLoading ? (
                                    <div className="share-loader">
                                        <Loader/>
                                    </div>
                                ) : (
                                    <>
                                        <img className="button-icon" src={`${IMAGES_PATH}share.png`} alt="download"/>
                                        <span className="label">{"Share"}</span>
                                    </>
                                )}
                            </button>
                            {this.state.shareImage && (
                                <Wrapper
                                    close={() => {
                                        this.toggleShare(false);
                                    }}
                                    classList="share-wrapper"
                                    title="Share">
                                    {SOCIAL_SHARE_ICONS.map((item, key) => {
                                        return (
                                            <a href={item.link + this.state.shareImage}
                                               key={key}
                                               rel="noopener noreferrer"
                                               target="_blank"
                                               className="social-item">
                                                <i className={`${item.icon} social-icon`}/>
                                                <span className="social-label">{item.name}</span>
                                            </a>
                                        )
                                    })}
                                </Wrapper>
                            )}
                        </div>
                    </div>
                </div>
                <div className="draw-tools-panel">
                    <div className="colors-panel">
                        <p className="color-label">{"Change background color"}</p>
                        <div className="colors-list">
                            {DRAW_DEFAULT_COLORS.map((color, index) => {
                                return (
                                    <button
                                        key={index}
                                        onClick={() => {
                                            this.setBackColor(color.hex);
                                        }}
                                        className="color-item"
                                        style={{backgroundColor: color.hex}}/>
                                )
                            })}
                            <button
                                onClick={() => {
                                    this.toggleColor(true)
                                }}
                                className="new-color">
                                <i className="fa fa-plus"/>
                                {this.state.showColor && (
                                    <Wrapper
                                        close={() => {
                                            this.toggleColor(false);
                                        }}
                                        classList="color-picker">
                                        <ChromePicker
                                            color={this.state.backColor}
                                            disableAlpha={true}
                                            onChangeComplete={(color) => {
                                                this.handleChangeComplete(color);
                                            }}/>
                                    </Wrapper>
                                )}
                            </button>
                        </div>
                    </div>
                    <div className="order-buttons">
                        <div className="order-button"
                                onClick={() => {
                                    this.handleOpenEditModal();
                                }}>
                            <span className="icon"><i className="fa fa-pencil-alt"/></span>
                            <span>{"You can order artist to edit this portrait"}</span>
                        </div>
                        <div className="order-button"
                                onClick={() => {
                                    this.handleOpenOrderModal();
                                }}>
                            <span className="icon"><i className="fa fa-shopping-cart"/></span>
                            <span>{"You can order products with this portrait"}</span>
                        </div>
                        <div className="order-button"
                                onClick={() => {
                                    this.handleOpenApiRequestModal();
                                }}>
                            <span className="icon"><i className="fa fa-cog"/></span>
                            <span>{"Request for API"}</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Drawer;
