import React, {Component, createRef} from "react";
import './index.scss';

class Wrapper extends Component {
    constructor(props) {
        super(props);
        this.wrapperRef = createRef();
        this.handleClose = this.handleClose.bind(this);
        this.handleWrapperClick = this.handleWrapperClick.bind(this);
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClose);
        this.wrapperRef.current.addEventListener('click', this.handleWrapperClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClose);
        this.wrapperRef.current.removeEventListener('click', this.handleWrapperClick);
    }

    handleClose() {
        if (typeof this.props.close === 'function') {
            this.props.close();
        }
    }

    handleWrapperClick(e) {
        e.stopPropagation();
    }

    render() {
        let className = `wrapper`;

        if (this.props.classList) {
            className += ` ${this.props.classList}`;
        }

        return (
            <div ref={this.wrapperRef}
                 className={className}>
                {this.props.title && (
                    <div className="wrapper-header">
                        <span className="wrapper-title">{this.props.title}</span>
                        <i onMouseDown={(e) => {
                                this.handleClose(e);
                            }}
                            className="fas fa-times close-button"/>
                    </div>
                )}
                <div className="wrapper-body">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Wrapper;


