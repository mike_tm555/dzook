import React, {Component, createRef} from "react";
import './index.scss';

class Modal extends Component {
    constructor(props) {
        super(props);
        this.modalRef = createRef();
    }

    handleClose() {
        if (typeof this.props.close === 'function') {
            this.props.close();
        }
    }

    handleWrapperClick(e) {
        e.stopPropagation();
    }

    render() {
        let className = `modal`;

        if (this.props.classList) {
            className += ` ${this.props.classList}`;
        }

        const modalStyles = {
            width: '80vw',
        };

        if (this.props.width) {
            modalStyles.width = this.props.width;
        }

        if (this.props.height) {
            modalStyles.height = this.props.height;
        }

        if (this.props.maxWidth) {
            modalStyles.maxWidth = this.props.maxWidth;
        }

        if (this.props.maxHeight) {
            modalStyles.maxHeight = this.props.maxHeight;
        }

        return (
            <div className="modal-overlay"
                 onClick={(e) => {
                     this.handleClose(e);
                 }}>
                <div onClick={(e) => {
                    this.handleWrapperClick(e);
                }}
                     style={modalStyles}
                     className={className}>
                    {this.props.title && (
                        <div className="modal-header">
                            <span className="modal-title">{this.props.title}</span>
                        </div>
                    )}

                    <button className="close-modal"
                            onMouseDown={(e) => {
                                this.handleClose(e);
                            }}>
                        <i className="fas fa-times"/>
                    </button>

                    <div className="modal-body">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;


