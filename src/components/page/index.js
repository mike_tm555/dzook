import React, {useEffect} from "react";
import './index.scss'
import Header from "../header";
import Footer from "../footer";

function Page(props) {
    useEffect(() => {
        document.documentElement.scrollTop = 0;
    }, []);

    return (
        <div className={`page page-container ${props.classList}`}>
            <div className="page-header">
                <div className="page-wrapper">
                    {props.header ?? <Header logo={false}/>}
                </div>
            </div>
            <div className="page-body">
                <div className="page-wrapper">
                    {props.body || props.children}
                </div>
            </div>
            <div className="page-footer">
                <div className="page-wrapper">
                    {props.footer ?? <Footer/>}
                </div>
            </div>
        </div>
    )
}

export default Page;
