import React, {useCallback, useState} from "react";
import Form from "../form";
import {EDIT_FIELDS, EDIT_HOST} from "../../utils/constatns";
import Modal from "../modal";
import {Post} from "../../utils/helpers";
import './index.scss';

function Edit(props) {
    const [result, setResult] = useState(false);

    const handleSubmit = useCallback((data) => {
        Post(EDIT_HOST, {
            ...data,
        }).then((data) => {
            setResult({
                type: 'success',
                message: 'Thank you for the request. We will respond to you within 24 hours.'
            });
        }).catch(e => {
            setResult({
                type: 'error',
                message: 'Oops, something went wrong, please try again.\nPlease fill all required fields.'
            });
        });
    }, []);

    const toggleModal = useCallback((status) => {
        if (typeof props.toggleModal === 'function') {
            props.toggleModal(status);
        }
    }, [props]);

    const fields = EDIT_FIELDS.map(field => {
        if (field.name === 'image') {
            field.value = props.image;
        }

        return field;
    });

    return (
        <Modal
            width={"70vw"}
            maxHeight={"88vh"}
            maxWidth={"800px"}
            classList="edit-modal"
            close={() => {
                toggleModal(false);
            }}>
            <div className="row">
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <Form
                        showCancel={true}
                        title={"Write us an email and order \"Magic Hand\"! Artists can edit your portrait the way you want."}
                        cancel={() => {
                            toggleModal(false);
                        }}
                        submit={(data) => {
                            handleSubmit(data);
                        }}
                        message={result && (<p className={"form-result " + result.type}>{result.message}</p>)}
                        fields={fields}/>
                </div>
                <div className="col col-large-6 col-medium-12 col-small-12">
                    <div className="banner-box">
                    <div className="edit-banner-wrapper">
                            <h3 className="banner-title">{"Hey! If you want my artist-friends can edit your portrait, just write me:)"}</h3>
                            <div className="edit-banner"/>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default Edit;
