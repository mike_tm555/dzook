import React from "react";
import {Link} from "react-router-dom";
import './index.scss'
import {
    FOOTER_NAVIGATION_ITEMS,
    SOCIAL_PAGES_ICONS
} from "../../utils/constatns";

function Footer(props) {
    return (
        <div className="footer">
            <div className="row">
                <div className="col col-large-3 col-medium-3 col-small-12">
                </div>
                <div className="col col-large-9 col-medium-9 col-small-12">
                    <div className="footer-wrapper">
                        <div className="social-icons">
                            {SOCIAL_PAGES_ICONS.map((item, index) => {
                                return (
                                    <a key={index}
                                       rel="noopener noreferrer"
                                       target="_blank"
                                       className="social-nav-item"
                                       href={item.link}>
                                        {item.icon ? (<i className={item.icon}/>) : (<img alt={item.name} src={item.image}/>)}
                                    </a>
                                )
                            })}
                        </div>
                        <div className="navigation">
                            {FOOTER_NAVIGATION_ITEMS.map((item, index) => {
                                return item.tag ? (
                                    <a key={index}
                                       rel="noopener noreferrer"
                                       target="_blank"
                                       className="nav-item"
                                       href={item.link}>
                                        {item.name}
                                    </a>
                                ) :(
                                    <Link key={index}
                                          className="nav-item"
                                          to={item.link}>
                                        {item.name}
                                    </Link>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;
