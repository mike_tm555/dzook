import React from "react";
import './index.scss';

function Content(props) {
    return (
        <div className="page-content">
            {props.title && (<h1>{props.title}</h1>)}
            {props.image ? (
                <div className="row">
                    <div className="col col-large-8 col-medium-6 col-small-12"
                         dangerouslySetInnerHTML={{__html: props.content}}/>
                    <div className="col col-large-4 col-medium-6 col-small-12">
                        <img src={props.image} alt={props.title}/>
                    </div>
                </div>
            ) : (
                <div className="col col-large-12 col-medium-12 col-small-12"
                     dangerouslySetInnerHTML={{__html: props.content}}/>
            )}
        </div>
    )
}

export default Content;
