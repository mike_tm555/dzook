export function Request(url, method = 'GET', params, headers) {
    if (method === 'POST') {
        return Get(url, params, headers);
    }

    return Post(url, params, headers);
}

export function Post(url, params, headers) {
    return fetch(url, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...headers
        },
        method: 'POST',
        body: JSON.stringify(params)
    }).then(response => {
        return response.json();
    }).then(json => {
        return json;
    })
}

export function Get(url, params, headers) {
    const fetchURL = url + createQueryString(params);
    return fetch(fetchURL, {
        headers: headers
    }).then(response => {
        return response.json();
    }).then(json => {
        return json;
    })
}

export function Html(url) {
    return fetch(url).then(response => {
        return response.text();
    }).then(text => {
        return text;
    })
}



export function createQueryString(params) {
    let queryString = '';

    if (Object.keys(params)) {
        queryString = '?';

        const paramsMap = Object.keys(params).map((key) => {
            return `${key}=${params[key]}`;
        });

        queryString += paramsMap.join('&');
    }

    return queryString;
}
