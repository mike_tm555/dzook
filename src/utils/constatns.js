export const HEADER_NAVIGATION_ITEMS = [

];

export const FOOTER_NAVIGATION_ITEMS = [
    {
        name: "About",
        link: "/about"
    },
    {
        name: "Feedback",
        link: "/feedback"
    },
    {
        name: "Subscription",
        link: "https://mailchi.mp/1e3d99666c0d/dzookselfieapp",
        tag: true
    },
    {
        name: "Terms and Conditions",
        link: "/terms"
    },
    {
        name: "Privacy Policy",
        link: "/privacy"
    },
    {
        name: "Contact Us",
        link: "/contact"
    }
];

export const PAGES_NAVIGATION_ITEMS = [
    {
        name: "Privacy Policy",
        link: "/privacy"
    },
    {
        name: "Terms and Conditions",
        link: "/terms"
    },
    {
        name: "Disclaimer",
        link: "/disclaimer"
    },
    {
        name: "Cookie Policy",
        link: "/cookie"
    },
    {
        name: "Acceptable Use Policy",
        link: "/acceptable-policy"
    },
    {
        name: "Terms and Conditions for \"dzook\" mobile app",
        link: "/app-terms"
    },
    {
        name: "Privacy Policy for \"dzook\" mobile app",
        link: "/app-privacy"
    }
];

export const SOCIAL_PAGES_ICONS = [
    {
        icon: "fab fa-facebook",
        link: "https://www.facebook.com/dzookapp/"
    },
    {
        icon: "fab fa-instagram",
        link: "https://www.instagram.com/dzook_app/"
    },
    {
        icon: "fab fa-twitter",
        link: "https://twitter.com/3_dzook"
    },
    {
        icon: "fab fa-youtube",
        link: "https://www.youtube.com/channel/UCQzecEFTE6N4F34IoNIHd0w"
    },
    {
        icon: "fab fa-linkedin",
        link: "https://www.linkedin.com/company/3-dzook/"
    },
    {
        image: "/assets/images/tiktok.png",
        link: "https://vm.tiktok.com/73tpAS/"
    }
];

export const SHARE_ROUTE = 'https://dzook.ai/image/';

export const SOCIAL_SHARE_ICONS = [
    {
        icon: "fab fa-facebook",
        name: "Facebook",
        link: "https://www.facebook.com/sharer/sharer.php?u=" + SHARE_ROUTE
    },
    {
        icon: "fas fa-envelope",
        name: "E-mail",
        link: "mailto:info@example.com?&subject=&body=" + SHARE_ROUTE
    },
    {
        icon: "fab fa-linkedin",
        name: "Linkedin",
        link: "https://www.linkedin.com/shareArticle?mini=true&title=dzook.ai.&summary=dzook.ai.&url=" + SHARE_ROUTE
    },
    {
        icon: "fab fa-twitter",
        name: "Twitter",
        link: "https://twitter.com/intent/tweet/?text=https://dzook.ai.&url=" + SHARE_ROUTE
    }
];

export const DRAW_HOST = 'https://dzook.ai/image/redesign';
export const UPLOAD_HOST = 'https://dzook.ai/image/upload';
export const FEEDBACK_HOST = 'https://dzook.ai/email/feedback';
export const CONTACT_HOST = 'https://dzook.ai/email/contact';
export const ORDER_HOST = 'https://dzook.ai/email/order';
export const EDIT_HOST = 'https://dzook.ai/email/order';
export const API_REQUEST_HOST = 'https://dzook.ai/email/api';

export const IMAGES_PATH = '/assets/images/';

export const DRAW_DEFAULT_COLORS = [
    {
        hex: '#ff7092'
    },
    {
        hex: '#24e3d7'
    },
    {
        hex: '#ffffff'
    },
    {
        hex: '#3cc846'
    },
    {
        hex: '#000000'
    },
    {
        hex: '#2464e3'
    },
    {
        hex: '#970efe'
    },
    {
        hex: '#fff716'
    },
    {
        hex: '#747474'
    }
];

export const FEEDBACK_FIELDS = [
    {
        type: 'text',
        name: 'name',
        label: 'Your name *',
        required: true,
    },
    {
        type: 'email',
        name: 'email',
        label: 'Your e-mail *',
        required: true,
    } ,
    {
        type: 'textarea',
        name: 'feedback',
        label: 'Your Feedback *',
        required: true,
    }
];

export const CONTACT_US_FIELDS = [
    {
        type: 'text',
        name: 'name',
        label: 'Your name *',
        required: true,
    },
    {
        type: 'email',
        name: 'email',
        label: 'Your e-mail *',
        required: true,
    },
    {
        type: 'tel',
        name: 'phone',
        label: 'Your phone',
    } ,
    {
        type: 'textarea',
        name: 'description',
        label: 'Your Message *',
        required: true,
    }
];

export const ORDER_FIELDS = [
    {
        type: 'text',
        name: 'name',
        label: 'Your name *',
        required: true,
    },
    {
        type: 'email',
        name: 'email',
        label: 'Your e-mail *',
        required: true,
    },
    {
        type: 'tel',
        name: 'phone',
        label: 'Your phone',
    },
    {
        type: 'textarea',
        name: 'description',
        label: 'Your Message *',
        required: true,
    },
    {
        type: 'hidden',
        name: 'image',
        label: 'Image',
        required: true,
    },
    {
        type: 'hidden',
        name: 'type',
        value: '1',
        label: 'Type',
        required: true,
    }
];

export const EDIT_FIELDS = [
    {
        type: 'text',
        name: 'name',
        label: 'Your name *',
        required: true,
    },
    {
        type: 'email',
        name: 'email',
        label: 'Your e-mail *',
        required: true,
    },
    {
        type: 'tel',
        name: 'phone',
        label: 'Your phone',
    },
    {
        type: 'textarea',
        name: 'description',
        label: 'Your Message *',
        required: true,
    },
    {
        type: 'hidden',
        name: 'image',
        label: 'Image',
        required: true,
    },
    {
        type: 'hidden',
        name: 'type',
        value: '2',
        label: 'Type',
        required: true,
    }
];

export const ORDER_ITEMS = [
    {
        name: "T-Shirt",
        image: "shirt.png"
    },
    {
        name: "Phone Case",
        image: "case.png"
    },
    {
        name: "Termo Mug",
        image: "mug.png",
    },
    {
        name: "Tote Bag",
        image: "totebag.png"
    }
];

export const API_REQUEST_FIELDS = [
    {
        type: 'text',
        name: 'name',
        label: 'Your name *',
        required: true,
    },
    {
        type: 'email',
        name: 'email',
        label: 'Your e-mail *',
        required: true,
    } ,
    {
        type: 'textarea',
        name: 'description',
        label: 'Your Wish *',
        required: true,
    }
];
