import React, {Component} from "react";
import Page from "../../components/page";
import './index.scss';
import { Html} from "../../utils/helpers";
import Content from "../../components/content";
import Navigation from "../../components/navigation";
import Logo from "../../components/logo";

class AppTerms extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: null
        };
    }

    componentDidMount() {
        Html('/assets/content/app-terms.html', {}).then((data) => {
            this.setState({
                content: data
            });
        });
    }

    render() {
        return (
            <Page classList="app-terms-page">
                <div className="row">
                    <div className="col col-large-3 col-medium-12 col-small-12">
                        <Logo/>
                    </div>
                    <div className="col col-large-9 col-medium-12 col-small-12">
                        <Navigation/>
                        {this.state.content && (
                            <Content
                                title={"Terms and Conditions for \"dzook\" mobile app"}
                                content={this.state.content}/>
                        )}
                    </div>
                </div>
            </Page>
        )
    }
}

export default AppTerms;
