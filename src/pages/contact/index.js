import React, {useCallback, useState} from "react";
import Page from "../../components/page";
import './index.scss';
import Form from "../../components/form";
import {CONTACT_HOST, CONTACT_US_FIELDS} from "../../utils/constatns";
import {Post} from "../../utils/helpers";
import Logo from "../../components/logo";

function Contact() {
    const [result, setResult] = useState(false);

    const handleSubmit = useCallback((data) => {
        Post(CONTACT_HOST, {
            ...data,
        }).then((data) => {
            setResult({
                type: 'success',
                message: 'Your message has been sent successfully!'
            });
        }).catch(e => {
            setResult({
                type: 'error',
                message: 'Oops, something went wrong, please try again.\nPlease fill all required fields.'
            });
        });
    }, []);

    return (
        <Page classList="contact-page">
            <div className="row">
                <div className="col col-large-3 col-medium-12 col-small-12">
                    <Logo/>
                </div>
                <div className="col col-large-9 col-medium-12 col-small-12">
                    <div className="contact-us-wrapper">
                        <div className="row">
                            <div className="col col-large-6 col-medium-12 col-small-12">
                                <div className="contact-banner-wrapper">
                                    <div className="contact-banner"/>
                                </div>
                            </div>
                            <div className="col col-large-6 col-medium-12 col-small-12">
                                <div className="contact-form">
                                    <Form submitText={"Send"}
                                          submit={(data) => {
                                              handleSubmit(data);
                                          }}
                                          message={result && (<p className={"form-result " + result.type}>{result.message}</p>)}
                                          fields={CONTACT_US_FIELDS}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    )
}

export default Contact;
