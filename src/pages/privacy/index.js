import React, {Component} from "react";
import Page from "../../components/page";
import './index.scss';
import { Html} from "../../utils/helpers";
import Content from "../../components/content";
import Navigation from "../../components/navigation";
import Logo from "../../components/logo";

class Privacy extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: null
        };
    }

    componentDidMount() {
        Html('/assets/content/privacy-policy.html', {}).then((data) => {
            this.setState({
                content: data
            });
        });
    }

    render() {
        return (
            <Page classList="privacy-page">
                <div className="row">
                    <div className="col col-large-3 col-medium-12 col-small-12">
                        <Logo/>
                    </div>
                    <div className="col col-large-9 col-medium-12 col-small-12">
                        <Navigation/>
                        {this.state.content && (
                            <Content title={"Privacy Policy"} content={this.state.content}/>
                        )}
                    </div>
                </div>
            </Page>
        )
    }
}

export default Privacy;
