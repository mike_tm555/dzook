import React from "react";
import Page from "../../components/page";
import Editor from "../../components/editor";
import Header from "../../components/header";

function Home() {
    return (
        <Page classList="home-page"
              header={<Header logo={false}/>}>
            <Editor/>
        </Page>
    )
}

export default Home;
