import React, {Component} from "react";
import Page from "../../components/page";
import './index.scss';
import {Html} from "../../utils/helpers";
import Content from "../../components/content";
import {IMAGES_PATH} from "../../utils/constatns";
import Logo from "../../components/logo";

class About extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: null
        };
    }

    componentDidMount() {
        Html('/assets/content/about-us.html', {}).then((data) => {
            this.setState({
                content: data
            });
        });
    }

    render() {
        return (
            <Page classList="privacy-page">
                <div className="row">
                    <div className="col col-large-3 col-medium-12 col-small-12">
                        <Logo/>
                    </div>
                    <div className="col col-large-9 col-medium-12 col-small-12">
                        {this.state.content && (
                            <Content
                                content={this.state.content}
                                image={`${IMAGES_PATH}about-us.png`}/>
                        )}
                    </div>
                </div>
            </Page>
        )
    }
}

export default About;
