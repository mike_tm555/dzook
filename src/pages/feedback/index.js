import React, {useCallback, useState} from "react";
import Page from "../../components/page";
import './index.scss';
import Form from "../../components/form";
import { FEEDBACK_FIELDS, FEEDBACK_HOST } from "../../utils/constatns";
import {Post} from "../../utils/helpers";
import Logo from "../../components/logo";

function FeedBack() {
    const [result, setResult] = useState(false);
    const handleSubmit = useCallback((data) => {
        Post(FEEDBACK_HOST, {
            ...data,
        }).then((data) => {
            setResult({
                type: 'success',
                message: 'Your feedback has been sent successfully!'
            });
        }).catch(e => {
            setResult({
                type: 'error',
                message: 'Oops, something went wrong, please try again.\nPlease fill all required fields.'
            });
        });
    }, []);

    return (
        <Page classList="feedback-page">
            <div className="row">
                <div className="col col-large-3 col-medium-12 col-small-12">
                    <Logo/>
                </div>
                <div className="col col-large-9 col-medium-12 col-small-12">
                    <div className="feedback-wrapper">
                        <div className="row">
                            <div className="col col-large-6 col-medium-12 col-small-12">
                                <div className="feedback-banner-wrapper">
                                    <div className="feedback-banner"/>
                                </div>
                            </div>
                            <div className="col col-large-6 col-medium-12 col-small-12">
                                <div className="feedback-form">
                                    <Form
                                        submitText={"Send"}
                                        submit={(data) => {
                                            handleSubmit(data);
                                        }}
                                        message={result && (<p className={"form-result " + result.type}>{result.message}</p>)}
                                        fields={FEEDBACK_FIELDS}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Page>
    )
}

export default FeedBack;
