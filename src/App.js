import React from 'react';
import './App.scss';
import Home from "./pages/home";
import {
    BrowserRouter,
    Switch,
    Route,
} from "react-router-dom";
import About from "./pages/about";
import Terms from "./pages/terms";
import Privacy from "./pages/privacy";
import FeedBack from "./pages/feedback";
import Contact from "./pages/contact";
import CookiePolicy from "./pages/cookie";
import AppPrivacy from "./pages/app-privacy";
import AppTerms from "./pages/app-terms";
import Disclaimer from "./pages/disclaimer";
import AcceptablePolicy from "./pages/acceptable";

function App() {
  return (
    <div className="App light-theme">
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/about">
                    <About />
                </Route>
                <Route exact path="/terms">
                    <Terms />
                </Route>
                <Route exact path="/privacy">
                    <Privacy />
                </Route>
                <Route exact path="/feedback">
                    <FeedBack />
                </Route>
                <Route exact path="/contact">
                    <Contact />
                </Route>
                <Route exact path="/cookie">
                    <CookiePolicy />
                </Route>
                <Route exact path="/app-privacy">
                    <AppPrivacy />
                </Route>
                <Route exact path="/app-terms">
                    <AppTerms />
                </Route>
                <Route exact path="/disclaimer">
                    <Disclaimer />
                </Route>
                <Route exact path="/acceptable-policy">
                    <AcceptablePolicy />
                </Route>
            </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
